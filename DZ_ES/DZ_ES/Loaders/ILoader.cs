﻿using System;
using System.Collections.Generic;
using DZ_ES.Additional;
using DZ_ES.Auditoriums;
using DZ_ES.Subject;
using DZ_ES.TeacherClass;

namespace DZ_ES.Loaders
{
    public interface ILoader
    {
        List<SubjectDescription> LoadDescriptions();
        List<Auditorium> LoadAuditoriums();
        List<Teacher> LoadTeachers();
        List<Duration<TimeSpan>> LoadTimePairs();
    }
}