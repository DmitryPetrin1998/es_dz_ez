﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DZ_ES.Additional;
using DZ_ES.Auditoriums;
using DZ_ES.Subject;
using DZ_ES.TeacherClass;
using DZ_ES.Utils;

namespace DZ_ES.Loaders.CsvLoader
{
    public class CsvLoader : ILoader
    {
        public string FileName { get; set; }
        public char Delimeter { get; set; }

        public List<SubjectDescription> LoadDescriptions()
        {
            var descriptions= File.ReadAllLines(FileName);

            var result = new List<SubjectDescription>(descriptions.Length);
            result.AddRange(descriptions.Select(description => description.Split(Delimeter))
                .Select(data => new SubjectDescription(data[0], Convert.ToInt32(data[1]), Convert.ToInt32(data[2]),
                Convert.ToInt32(data[3]), (AuditoriumType)Convert.ToInt32(data[4]), Convert.ToInt32(data[5]))));

            return result;
        }

        public List<Auditorium> LoadAuditoriums()
        {
            var auditoriums = File.ReadAllLines(FileName);

            var result = new List<Auditorium>(auditoriums.Length);
            result.AddRange(auditoriums.Select(auditorium => auditorium.Split(Delimeter))
                .Select(data => new Auditorium(Convert.ToInt32(data[0]), Convert.ToInt32(data[1]), 
                (AuditoriumType)Convert.ToInt32(data[2]))));

            return result;
        }

        public List<Teacher> LoadTeachers()
        {
            var teachers = File.ReadAllLines(FileName);

            var result = new List<Teacher>(teachers.Length);
            result.AddRange(teachers.Select(teacher => teacher.Split(Delimeter))
                .Select(data => new Teacher(data[0], data[1], 
                data.ToList().GetRange(2, data.Length - 2).ToArray())));
          
            return result;
        }

        public List<Duration<TimeSpan>> LoadTimePairs()
        {
            var pairs = File.ReadAllLines(FileName);

            var result = new List<Duration<TimeSpan>>(pairs.Length);
            result.AddRange(pairs.Select(pair => pair.Split(Delimeter))
                .Select(data => new Duration<TimeSpan>(data[0].ToTime(), data[1].ToTime())));
           
            return result;
        }
    }
}