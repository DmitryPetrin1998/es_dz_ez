﻿using System;
using DZ_ES.Utils;

namespace DZ_ES.Additional
{
    public class Duration<T>
    {
        public T Begin { get; set; }
        public T End { get; set; }
        public Duration(T begin, T end)
        {
            Begin = begin;
            End = end;
        }

        public override string ToString()
        {
            if (typeof(T) != typeof(TimeSpan))
            {
                return base.ToString();
            }
            var begin = Convert.ChangeType(Begin, new TimeSpan().GetType());
            var end = Convert.ChangeType(End, new TimeSpan().GetType());
            if (begin == null || end == null)
            {
                return base.ToString();
            }
            return
                $"{(TimeSpan)begin:hh\\:mm\\:ss} - {(TimeSpan)end:hh\\:mm\\:ss}";
        }
    }
}