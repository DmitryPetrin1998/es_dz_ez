﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DZ_ES.Additional
{
    public class TimePairsContainer
    {
        private const int DefaultMaxPairsCount = 7;

        private readonly uint _maxPairsCount;
        private readonly List<Duration<TimeSpan>> _pairs;

        public Duration<TimeSpan> GetDurationForPair(int number)
        {
            return number > _pairs.Count ? _pairs.Last() : _pairs[number];
        }

        public TimePairsContainer(uint maxPairsCount = DefaultMaxPairsCount, List<Duration<TimeSpan>> pairs = null)
        {
            _maxPairsCount = maxPairsCount;
            if (pairs != null)
            {
                _pairs = pairs;
                return;
            }
            _pairs = new List<Duration<TimeSpan>>((int)_maxPairsCount);
        }

        public void AddPair(Duration<TimeSpan> time)
        {
            if (_pairs.Count >= _maxPairsCount)
            {
                return;
            }
            _pairs.Add(time);
            _pairs.Sort();
        }
    }
}