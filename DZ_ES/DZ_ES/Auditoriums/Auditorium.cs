﻿namespace DZ_ES.Auditoriums
{
    public class Auditorium
    {
        public int Number { get; set; }
        public int Capacity { get; set; }
        public AuditoriumType Type { get; set; }

        public Auditorium(int number, int capacity, AuditoriumType type)
        {
            Number = number;
            Capacity = capacity;
            Type = type;
        }

        public override string ToString()
        {
            return $"{Number}";
        }
    }
}