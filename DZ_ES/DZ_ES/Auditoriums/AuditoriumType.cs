﻿namespace DZ_ES.Auditoriums
{
    public enum AuditoriumType
    {
        Lecture,
        Seminar,
        Laboratory
    }
}