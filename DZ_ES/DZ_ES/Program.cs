﻿using System;
using DZ_ES.EventHandlers.AuditoriumConflictHandler;
using DZ_ES.EventHandlers.SubjectConflictHandler;
using DZ_ES.EventHandlers.TeacherConflictHandler;
using DZ_ES.Loaders.CsvLoader;
using DZ_ES.Shedule;
using DZ_ES.Writers;

namespace DZ_ES
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var csvLoader = new CsvLoader
            {
                FileName = "D:\\ES_LABS\\DZ\\data\\time_pairs.csv",
                Delimeter = ';'
            };
            var timePairs = csvLoader.LoadTimePairs();

            csvLoader.FileName = "D:\\ES_LABS\\DZ\\data\\teachers.csv";
            var teachers = csvLoader.LoadTeachers();
          
            csvLoader.FileName = "D:\\ES_LABS\\DZ\\data\\descriptions.csv";
            var descriptions = csvLoader.LoadDescriptions();

            csvLoader.FileName = "D:\\ES_LABS\\DZ\\data\\auditoriums.csv";
            var auditoriums = csvLoader.LoadAuditoriums();

            var generator = new SheduleGenerator(descriptions, auditoriums, teachers, timePairs);
            generator.SubjectConflict += SubjectConflictEventHandler.Handle;
            generator.AuditoriumConflict += AuditoriumConflictEventHandler.Handle;
            generator.TeacherConflict += TeacherConflictEventHandler.Handle;

            var shedule = generator.Generate();

            if (shedule != null)
            {
                var sheduleWriter = new SheduleWriter(shedule);
                sheduleWriter.WriteOnConsole(generator.PairsContainer);
                sheduleWriter.FileName = "D:\\ES_LABS\\DZ\\data\\shedule.txt";
                sheduleWriter.WriteToTxtFile(generator.PairsContainer);
                return;
            }
            Console.WriteLine("Невозможно составить расписание. Слишком много пар.");
            Console.ReadLine();
        }
    }
}