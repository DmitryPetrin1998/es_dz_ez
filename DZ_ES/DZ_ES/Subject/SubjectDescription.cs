﻿using DZ_ES.Auditoriums;

namespace DZ_ES.Subject
{
    public class SubjectDescription
    {
        public string Name { get; set; }
        public int Priority { get; set; } 
        public int PairsInWeek { get; set; }
        public int PairNumber { get; set; }
        public AuditoriumType PreferredAuditoriumType { get; set; }
        public int PreferredAuditoriumNumber { get; set; }

        public SubjectDescription(string name, int priority, int pairsInWeek, int pairNumber, 
            AuditoriumType preferredAuditoriumType, int preferredAuditoriumNumber)
        {
            Name = name;
            Priority = priority;
            PairsInWeek = pairsInWeek;
            PairNumber = pairNumber;
            PreferredAuditoriumType = preferredAuditoriumType;
            PreferredAuditoriumNumber = preferredAuditoriumNumber;
        }
    }
}