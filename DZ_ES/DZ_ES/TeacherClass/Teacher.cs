using System;
using System.Globalization;
using System.Linq;

namespace DZ_ES.TeacherClass
{
    public class Teacher
    {
        public string FullName { get; set; }
        public string Rank { get; set; }
        public string[] SubjectsCompetitions { get; set; }

        public Teacher(string fullName, string rank, string[] subjectsCompetitions)
        {
            FullName = fullName;
            Rank = rank;
            SubjectsCompetitions = subjectsCompetitions;
        }

        public override string ToString()
        {
            return $"{FullName} : {string.Join(",", SubjectsCompetitions)}";
        }
    }
}