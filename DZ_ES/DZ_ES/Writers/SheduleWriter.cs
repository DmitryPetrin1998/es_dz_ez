﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DZ_ES.Additional;
using DZ_ES.Shedule;

namespace DZ_ES.Writers
{
    public class SheduleWriter
    {
        private readonly List<List<SheduleEntity>> _shedule;

        public string FileName { get; set; }

        public SheduleWriter()
        {
        }

        public SheduleWriter(IEnumerable<IEnumerable<SheduleEntity>> shedule)
        {
            var sheduleTemp = shedule.ToList();
            _shedule = new List<List<SheduleEntity>>(sheduleTemp.Count);
            for (var i = 0; i < sheduleTemp.Count; i++)
            {
                _shedule.Add(new List<SheduleEntity>());
            }
            for (var i = 0; i < sheduleTemp.Count; i++)
            {
                _shedule[i] = sheduleTemp[i].ToList();
            } 
        }

        public void WriteOnConsole(TimePairsContainer container)
        {
            Console.Clear();
            for (var i = 0; i < _shedule.Count; i++)
            {
                var pairNumber = 1;
                for (var j = 0; j < _shedule[i].Count; j++)
                {
                    if (_shedule[i][j] == null && i > 0)
                    {
                        Console.WriteLine($"{pairNumber, -2} {SheduleGenerator.GetDay(i), -13} {container.GetDurationForPair(j), -22}");
                        pairNumber++;
                        continue;
                    }

                    Console.WriteLine(_shedule[i][j]);
                    pairNumber++;
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }

        public void WriteToTxtFile(TimePairsContainer container)
        {
            using (var file = new StreamWriter(FileName))
            {
                for (var i = 0; i < _shedule.Count; i++)
                {
                    var pairNumber = 1;
                    for (var j = 0; j < _shedule[i].Count; j++)
                    {
                        if (_shedule[i][j] == null && i > 0)
                        {
                            file.WriteLine($"{pairNumber,-2} {SheduleGenerator.GetDay(i),-13} {container.GetDurationForPair(j),-22}");
                            pairNumber++;
                            continue;
                        }

                        file.WriteLine(_shedule[i][j]);
                        pairNumber++;
                    }
                    file.WriteLine();
                }
            }        
        }
    }
}