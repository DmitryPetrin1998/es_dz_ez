﻿using System;
using System.Globalization;

namespace DZ_ES.Utils
{
    public static class TimeConvertExtension
    {
        public static TimeSpan ToTime(this string time)
        {
            return TimeSpan.ParseExact(time, "hh\\:mm\\:ss", CultureInfo.InvariantCulture);
        }

        public static string String(this TimeSpan time)
        {
            return $"{time.Hours}:{time.Minutes}";
        }

    }
}