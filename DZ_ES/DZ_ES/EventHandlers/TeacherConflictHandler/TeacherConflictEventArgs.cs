﻿using System;
using System.Collections.Generic;
using DZ_ES.Subject;
using DZ_ES.TeacherClass;

namespace DZ_ES.EventHandlers.TeacherConflictHandler
{
    public class TeacherConflictEventArgs : EventArgs
    {
        public List<Teacher> Teachers { get; }
        public SubjectDescription Subject { get; }
        public int DayNumber { get; }
        public int PairNumber { get; }
        
        public TeacherConflictEventArgs(List<Teacher> teachers, SubjectDescription subject, int dayNumber, int pairNumber)
        {
            Teachers = teachers;
            Subject = subject;
            PairNumber = pairNumber;
            DayNumber = dayNumber;
        }
    }
}