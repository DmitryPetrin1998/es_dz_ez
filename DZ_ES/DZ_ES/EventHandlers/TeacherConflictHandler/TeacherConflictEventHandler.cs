﻿using System;
using System.Collections.Generic;
using DZ_ES.Shedule;
using DZ_ES.TeacherClass;

namespace DZ_ES.EventHandlers.TeacherConflictHandler
{
    public static class TeacherConflictEventHandler
    {
        public static Teacher Handle(SheduleGenerator generator, TeacherConflictEventArgs args)
        {
            Console.Clear();
            Console.WriteLine($"Нет преподавателей с необходимой специальностью для предмета {args.Subject.Name}. " +
                              $"{args.PairNumber} пара {SheduleGenerator.GetDay(args.DayNumber)}. Выберите вручную:");
            Console.WriteLine();

            for (var i = 0; i < args.Teachers.Count; i++)
            {
               Console.WriteLine($"{i + 1}. {args.Teachers[i]}");
            }
            Console.WriteLine();

            int answer;
            bool isParse;
            do
            {
                Console.Write("Выбор: ");
                isParse = int.TryParse(Console.ReadLine(), out answer);

                if (answer > args.Teachers.Count || answer < 1 || !isParse)
                {
                    Console.WriteLine("Вы ввели неккоректное число. Введите еще раз ");
                }

            } while (answer > args.Teachers.Count || answer < 1 || !isParse);

            return args.Teachers[--answer];
        }
    }
}