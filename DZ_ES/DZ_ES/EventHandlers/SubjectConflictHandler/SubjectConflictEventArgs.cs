﻿using System;
using System.Collections.Generic;
using DZ_ES.Additional;
using DZ_ES.Auditoriums;
using DZ_ES.Shedule;
using DZ_ES.Subject;
using DZ_ES.TeacherClass;

namespace DZ_ES.EventHandlers.SubjectConflictHandler
{
    public class SubjectConflictEventArgs : EventArgs
    {
        public List<List<SheduleEntity>> Shedule { get; }
        public TimePairsContainer Container { get; }
        public List<Teacher> Teachers { get; }
        public List<Auditorium> Auditoriums { get; }
        public SubjectDescription Subject { get; }
        public int DayNumber { get; }
        public int PairNumber { get; }

        public SubjectConflictEventArgs(List<List<SheduleEntity>> shedule, TimePairsContainer container, 
            List<Teacher> teachers, List<Auditorium> auditoriums, 
            SubjectDescription subject, int dayNumber, int pairNumber)
        {
            Shedule = shedule;
            Container = container;
            Teachers = teachers;
            Auditoriums = auditoriums;
            Subject = subject;
            DayNumber = dayNumber;
            PairNumber = pairNumber;
        }
    }
}