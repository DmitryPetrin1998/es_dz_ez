﻿using System;
using System.Collections.Generic;
using DZ_ES.EventHandlers.AuditoriumConflictHandler;
using DZ_ES.EventHandlers.TeacherConflictHandler;
using DZ_ES.Shedule;
using DZ_ES.Subject;

namespace DZ_ES.EventHandlers.SubjectConflictHandler
{
    public static class SubjectConflictEventHandler
    {
        public static void Handle(SheduleGenerator generator, SubjectConflictEventArgs args)
        {
            Console.Clear();
            Console.WriteLine($"Обнаружено совпадение предметов на одной паре: {SheduleGenerator.GetDay(args.DayNumber)}, " +
                              $"{args.PairNumber + 1} пара");
            Console.WriteLine($"Конфликтующие предметы: \"{args.Shedule[args.DayNumber][args.PairNumber].Subject.Name}\" " +
                              $"и \"{args.Subject.Name}\"");
            Console.Write("Разрешить конфликт автоматически (y/n): ");
            var response = Console.ReadLine();

            if (response != null && response.ToLower() == "n")
            {
                Console.Clear();
                Console.WriteLine($"Конфликтующие предметы: \"{args.Shedule[args.DayNumber][args.PairNumber].Subject.Name}\" " +
                                  $"и \"{args.Subject.Name}\"");
                Console.WriteLine();
                WriteFreePairsOnConsole(generator, args.Shedule);

                ManuallyResolve(generator, args);
                return;
            }
            AutomaticResolve(generator, args);
        }

        private static int ReadDayNumber(IReadOnlyCollection<List<SheduleEntity>> shedule, SubjectDescription subject)
        {
            Console.Write($"Введите номер рабочего дня (1-{shedule.Count}) для \"{subject.Name}\": ");
            var response = Console.ReadLine();
            int day;
            bool isParse;
            do
            {
                isParse = int.TryParse(response, out day);
                if (isParse && day <= shedule.Count && day >= 1)
                {
                    continue;
                }
                Console.Write("Повторите ввод: ");
                response = Console.ReadLine();
                isParse = int.TryParse(response, out day);
            } while (!isParse || day > shedule.Count || day < 1);

            return --day;
        }

        private static int ReadPairNumber(IReadOnlyList<List<SheduleEntity>> shedule, SubjectDescription subject)
        {
            Console.Write($"Введите номер пары (1-{shedule[0].Count}) для \"{subject.Name}\": ");
            var response = Console.ReadLine();
            int pair;
            bool isParse;
            do
            {
                isParse = int.TryParse(response, out pair);
                if (isParse && pair <= shedule[0].Count && pair >= 1)
                {
                    continue;
                }
                Console.Write("Повторите ввод: ");
                response = Console.ReadLine();
                isParse = int.TryParse(response, out pair);
            } while (!isParse || pair > shedule[0].Count || pair < 1);

            return --pair;
        }

        private static void ManuallyResolve(SheduleGenerator generator, SubjectConflictEventArgs args)
        {
            var oldSheduleEntity = args.Shedule[args.DayNumber][args.PairNumber];

            var dayForFirst = ReadDayNumber(args.Shedule, args.Shedule[args.DayNumber][args.PairNumber].Subject);
            var pairForFirst = ReadPairNumber(args.Shedule, args.Shedule[args.DayNumber][args.PairNumber].Subject);
            Console.WriteLine();

            var dayForSecond = ReadDayNumber(args.Shedule, args.Subject);
            var pairForSecond = ReadPairNumber(args.Shedule, args.Subject);

            if (args.Shedule[dayForFirst][pairForFirst] != null)
            {
                generator.OnSubjectConflict(args.Shedule, oldSheduleEntity.Subject, dayForFirst, pairForFirst);
                return;
            }

            if (args.Shedule[dayForSecond][pairForSecond] != null)
            {
                generator.OnSubjectConflict(args.Shedule, args.Subject, dayForSecond, pairForSecond);
                return;
            }

            args.Shedule[dayForFirst][pairForFirst] = new SheduleEntity(SheduleGenerator.GetDay(dayForFirst), pairForFirst + 1,
                oldSheduleEntity.Duration, oldSheduleEntity.Subject, oldSheduleEntity.Teacher, oldSheduleEntity.Auditorium);

            args.Shedule[dayForSecond][pairForSecond] = new SheduleEntity(SheduleGenerator.GetDay(dayForSecond), pairForSecond + 1,
                args.Container.GetDurationForPair(pairForSecond), args.Subject, 
                generator.GetTeacher(new TeacherConflictEventArgs(args.Teachers, args.Subject, args.DayNumber, args.PairNumber)),
                generator.GetAuditorium(new AuditoriumConflictEventArgs(args.Auditoriums, args.Subject, 
                args.DayNumber, args.PairNumber)));
        }

        private static void AutomaticResolve(SheduleGenerator generator, SubjectConflictEventArgs args)
        {
            var oldSheduleEntity = args.Shedule[args.DayNumber][args.PairNumber];

            if (oldSheduleEntity.Subject.Priority < args.Subject.Priority)
            {
                args.Shedule[args.DayNumber][args.PairNumber] = new SheduleEntity(SheduleGenerator.GetDay(args.DayNumber), 
                    args.PairNumber, 
                    args.Container.GetDurationForPair(args.PairNumber), args.Subject, 
                    generator.GetTeacher(new TeacherConflictEventArgs(args.Teachers, args.Subject, args.DayNumber, args.PairNumber)), 
                    generator.GetAuditorium(new AuditoriumConflictEventArgs(args.Auditoriums, args.Subject, 
                    args.DayNumber, args.PairNumber)));

                SetSheduleEntity(generator, new SubjectConflictEventArgs(args.Shedule, args.Container, args.Teachers, 
                    args.Auditoriums, oldSheduleEntity.Subject, args.DayNumber, args.PairNumber));
                return;
            }
            SetSheduleEntity(generator, new SubjectConflictEventArgs(args.Shedule, args.Container, args.Teachers,
                args.Auditoriums, args.Subject, args.DayNumber, args.PairNumber));
        }

        private static void SetSheduleEntity(SheduleGenerator generator, SubjectConflictEventArgs args)
        {
            for (var pair = 0; pair < args.Shedule[0].Count; pair++)
            {
                for (var day = 0; day < args.Shedule.Count; day++)
                {
                    if (day + 1 == args.DayNumber && pair + 1 == args.PairNumber)
                    {
                        continue;
                    }
                    if (args.Shedule[day][pair] != null)
                    {
                        continue;
                    }
                    args.Shedule[day][pair] = new SheduleEntity(SheduleGenerator.GetDay(day), pair + 1, 
                        args.Container.GetDurationForPair(pair), 
                        args.Subject, generator.GetTeacher(new TeacherConflictEventArgs(args.Teachers, args.Subject, 
                        args.DayNumber, args.PairNumber)), generator.GetAuditorium(new AuditoriumConflictEventArgs(args.Auditoriums, 
                        args.Subject, args.DayNumber, args.PairNumber)));
                    return;
                }
            }
        }

        private static void WriteFreePairsOnConsole(SheduleGenerator generator, IReadOnlyList<List<SheduleEntity>> shedule)
        {
            Console.WriteLine("Свободные пары:");
            Console.WriteLine();
            for (var day = 0; day < shedule.Count; day++)
            {
                Console.Write($"{SheduleGenerator.GetDay(day)}: ");
                for (var pair = 0; pair < shedule[day].Count; pair++)
                {
                    if (shedule[day][pair] == null)
                    {
                        Console.Write($"{pair + 1} ");
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}