﻿using System;
using System.Collections.Generic;
using DZ_ES.Auditoriums;
using DZ_ES.Subject;

namespace DZ_ES.EventHandlers.AuditoriumConflictHandler
{
    public class AuditoriumConflictEventArgs : EventArgs
    {
        public List<Auditorium> Auditoriums { get; }
        public SubjectDescription Subject { get; }
        public int DayNumber { get; }
        public int PairNumber { get; }

        public AuditoriumConflictEventArgs(List<Auditorium> auditoriums, SubjectDescription subject, int dayNumber, int pairNumber)
        {
            Auditoriums = auditoriums;
            Subject = subject;
            DayNumber = dayNumber;
            PairNumber = pairNumber;
        }
    }
}