﻿using System;
using DZ_ES.Auditoriums;
using DZ_ES.Shedule;

namespace DZ_ES.EventHandlers.AuditoriumConflictHandler
{
    public static class AuditoriumConflictEventHandler
    {
        public static Auditorium Handle(SheduleGenerator generator, AuditoriumConflictEventArgs args)
        {
            Console.Clear();
            Console.WriteLine($"Нет подходящей для \"{args.Subject.Name}\" аудитории. {args.PairNumber} пара " +
                              $"{SheduleGenerator.GetDay(args.DayNumber)}. Выберите вручную:");
            Console.WriteLine();

            for (var i = 0; i < args.Auditoriums.Count; i++)
            {
                    Console.WriteLine(i + 1 + "." + " аудитория {0}" + " типа {1}", args.Auditoriums[i].Number,
                        args.Auditoriums[i].Type);
            }
            Console.WriteLine();     

            int answer;
            bool isParse;
            do
            {
                Console.Write("Выбор: ");
                isParse = int.TryParse(Console.ReadLine(), out answer);

                if (answer > args.Auditoriums.Count || answer < 1 || !isParse)
                {
                    Console.WriteLine("Вы ввели неккоректное число. Введите еще раз "); 
                }

            } while (answer > args.Auditoriums.Count || answer < 1 || !isParse); 

            return args.Auditoriums[--answer];
        }
    }
}