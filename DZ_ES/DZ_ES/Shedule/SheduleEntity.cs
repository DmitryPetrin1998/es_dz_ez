﻿using System;
using DZ_ES.Additional;
using DZ_ES.Subject;
using DZ_ES.TeacherClass;
using DZ_ES.Auditoriums;

namespace DZ_ES.Shedule
{
    public class SheduleEntity
    {
        public string WeekDay { get; set; }
        public int Pair { get; set; }
        public Duration<TimeSpan> Duration { get; set; }
        public SubjectDescription Subject { get; set; }
        public Teacher Teacher { get; set; }
        public Auditorium Auditorium { get; set; }

        public override string ToString()
        {
            return $"{Pair, -2} {WeekDay, -13} {Duration, -22} {Subject.Name, -25} {Teacher.FullName, -33} {Auditorium.Number, -5}";
        }

        public SheduleEntity()
        {
        }

        public SheduleEntity(string weekDay, int pair, Duration<TimeSpan> duration, SubjectDescription subject,
            Teacher teacher, Auditorium auditorium)
        {
            WeekDay = weekDay;
            Pair = pair;
            Duration = duration;
            Subject = subject;
            Teacher = teacher;
            Auditorium = auditorium;
        }
    }
}