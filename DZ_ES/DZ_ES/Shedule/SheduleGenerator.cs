﻿using System;
using System.Collections.Generic;
using System.Linq;
using DZ_ES.Additional;
using DZ_ES.Auditoriums;
using DZ_ES.EventHandlers.AuditoriumConflictHandler;
using DZ_ES.EventHandlers.SubjectConflictHandler;
using DZ_ES.EventHandlers.TeacherConflictHandler;
using DZ_ES.Subject;
using DZ_ES.TeacherClass;

namespace DZ_ES.Shedule
{
    public class SheduleGenerator
    {
        private const int DefaultWorkDays = 6;
        private const int DefaultMaxPairsCount = 7;
        private static readonly string[] WeekDays = 
            {"Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"};

        private readonly string[] _workDays;
        private readonly int _maxPairsCount;
        private readonly List<SubjectDescription> _descriptions;
        private readonly List<Auditorium> _auditoriums;
        private readonly List<Teacher> _teachers;
        private readonly List<Duration<TimeSpan>> _pairs;

        public TimePairsContainer PairsContainer { get; }

        public SheduleGenerator(IEnumerable<SubjectDescription> descriptions, IEnumerable<Auditorium> auditoriums, 
            IEnumerable<Teacher> teachers, IEnumerable<Duration<TimeSpan>> pairs,
            int workDaysCount = DefaultWorkDays, int maxPairsCount = DefaultMaxPairsCount)
        {
            _maxPairsCount = maxPairsCount;
            _descriptions = descriptions.ToList();
            _auditoriums = auditoriums.ToList();
            _teachers = teachers.ToList();
            _pairs = pairs.ToList();
            _workDays = new string[workDaysCount];
            InitWorkDays();
            PairsContainer = new TimePairsContainer((uint)_pairs.Count, _pairs);
        }

        public delegate Auditorium AuditoriumConflictEventHandler(SheduleGenerator generator, AuditoriumConflictEventArgs args);
        public delegate Teacher TeacherConflictEventHandler(SheduleGenerator generator, TeacherConflictEventArgs args);
        public delegate void SubjectConflictEventHandler(SheduleGenerator generator, SubjectConflictEventArgs args);

        public event AuditoriumConflictEventHandler AuditoriumConflict;
        public event TeacherConflictEventHandler TeacherConflict;
        public event SubjectConflictEventHandler SubjectConflict;

        public List<List<SheduleEntity>> Generate()
        {
            var shedule = InitShedule();

            if (_descriptions.Sum(subject => subject.PairsInWeek) > _maxPairsCount * _workDays.Length)
            {
                return null;
            }
            
            foreach (var subject in _descriptions)
            {
                var pairsInWeek = subject.PairsInWeek;
                for (var dayNumber = 0; pairsInWeek > 0; dayNumber++)
                {
                    if (shedule[dayNumber][subject.PairNumber - 1] == null)
                    {
                        shedule[dayNumber][subject.PairNumber - 1] = new SheduleEntity(_workDays[dayNumber], subject.PairNumber,
                            PairsContainer.GetDurationForPair(subject.PairNumber - 1),
                            subject, GetTeacher(new TeacherConflictEventArgs(_teachers, subject, dayNumber, subject.PairNumber - 1)),
                            GetAuditorium(new AuditoriumConflictEventArgs(_auditoriums, subject, dayNumber, subject.PairNumber - 1)));
                        pairsInWeek--;
                        continue;
                    }
                    OnSubjectConflict(shedule, subject, dayNumber, subject.PairNumber - 1);
                    pairsInWeek--;
                    if (dayNumber < _workDays.Length)
                    {
                        continue;
                    }
                    dayNumber = 0;
                    while (pairsInWeek > 0)
                    {
                        for (var pairNumber = 0; pairNumber < shedule[dayNumber].Count; pairNumber++)
                        {
                            if (shedule[dayNumber][pairNumber] != null)
                            {
                                continue;
                            }
                            shedule[dayNumber][pairNumber] = new SheduleEntity(_workDays[dayNumber], pairNumber + 1,
                                PairsContainer.GetDurationForPair(pairNumber),
                                subject, GetTeacher(new TeacherConflictEventArgs(_teachers, subject, dayNumber, pairNumber)), 
                                GetAuditorium(new AuditoriumConflictEventArgs(_auditoriums, subject, dayNumber, pairNumber)));
                            pairsInWeek--;
                            if (pairsInWeek == 0)
                            {
                                break;
                            }
                        }
                        dayNumber++;
                        if (dayNumber == _workDays.Length)
                        {
                            dayNumber = 0;
                        }
                    }
                }       
            }
            return shedule;
        }

        public void OnSubjectConflict(List<List<SheduleEntity>> shedule, SubjectDescription subject, 
            int dayNumber, int pairNumber)
        {
            SubjectConflict?.Invoke(this, 
                new SubjectConflictEventArgs(shedule, PairsContainer, _teachers, _auditoriums, subject, dayNumber, pairNumber));
        }

        public static string GetDay(int number)
        {
            return WeekDays[number];
        }

        private void InitWorkDays()
        {
            for (var i = 0; i < _workDays.Length; i++)
            {
                _workDays[i] = WeekDays[i];
            }
        }

        private List<List<SheduleEntity>> InitShedule()
        {
            var result = new List<List<SheduleEntity>>(_workDays.Length);
            for (var i = 0; i < _workDays.Length; i++)
            {
                var sheduleEntityList = new List<SheduleEntity>(_pairs.Count);
                for (var j = 0; j < _pairs.Count; j++)
                {
                    sheduleEntityList.Add(null);
                }
                result.Add(sheduleEntityList);
            }
            return result;
        }

        public Auditorium GetAuditorium(AuditoriumConflictEventArgs args)
        {
            var result = (from auditorium in args.Auditoriums
                         where auditorium.Number == args.Subject.PreferredAuditoriumNumber
                         select auditorium).FirstOrDefault();

            if (result == null || result.Type != args.Subject.PreferredAuditoriumType)
            {
                return AuditoriumConflict?.Invoke(this, args);
            }
            return result;
        }

        public Teacher GetTeacher(TeacherConflictEventArgs args)
        {
            foreach (var teacher in args.Teachers)
            {
                if (teacher.SubjectsCompetitions.Any(subj => subj == args.Subject.Name))
                {
                    return teacher;
                }
            }
            return TeacherConflict?.Invoke(this, args);
        }
    }
}